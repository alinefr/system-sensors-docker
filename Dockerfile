FROM python:3.9 AS compile-image
COPY requirements.txt .
RUN pip3 install --user -r requirements.txt

FROM python:3.9-slim
WORKDIR /app
RUN apt-get update \
  && apt-get install -y wireless-tools
COPY --from=compile-image /root/.local /root/.local
COPY src .
ENV PATH=/root/.local/bin:$PATH
CMD ["python3", "/app/system_sensors.py"]
